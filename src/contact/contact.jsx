import React from 'react';
import './contact.scss';
import {Gmaps, Marker} from 'react-gmaps';
import ScrollableAnchor from 'react-scrollable-anchor'
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import PhoneIcon from '@material-ui/icons/Phone';
import PlaceIcon from '@material-ui/icons/Place';
import EmailIcon from '@material-ui/icons/Email';
import CodeIcon from '@material-ui/icons/Code';

const coords = {
  lat: 48.8955316,
  lng: 2.3176649
};

const view = {
    lat: 48.89,
    lng: 2.3176649
  };

 
const params = {v: '3.exp', key: 'AIzaSyC8xgxgF6BWp2ugJU7TyLwvgBlsIiPzuw8'};
 
class Contact extends React.Component {
 
  onMapCreated(map) {
    map.setOptions({
      disableDefaultUI: true,
      zoom: 14,
      center: view   
    });
  }
 
  render() {
    return (
        <ScrollableAnchor id={'contact'} >
        <Grid container className="contact main-container">
            <Grid item xs={12}  >
                <h3>Contact</h3>
            </Grid>
            <div className="map-container">    
            <Gmaps
        width={'100%'}
        height={'600px'}
        lat={coords.lat}
        lng={coords.lng}
        zoom={12}
        loadingMessage={'Be happy'}
        params={params}
        onMapCreated={this.onMapCreated}>
        <Marker
          lat={coords.lat}
          lng={coords.lng}
          draggable={true}
          onDragEnd={this.onDragEnd} />
      </Gmaps>
      <Card className="card">
        <CardContent>
            <List>
                <ListItem button component="a" href="tel:06 51 71 57 48">
                    <ListItemIcon>
                        <PhoneIcon />
                    </ListItemIcon>
                    <ListItemText inset primary="06 51 71 57 48" />
                </ListItem>
                <ListItem button component="a" target="_blank" href="https://www.google.com/maps/place/8+Passage+Boulay,+75017+Paris/@48.8955608,2.3155817,17z/data=!3m1!4b1!4m5!3m4!1s0x47e66fa9b93871c1:0xc90016e30bd0aa5a!8m2!3d48.8955608!4d2.3177704">
                    <ListItemIcon>
                        <PlaceIcon />
                    </ListItemIcon>
                    <ListItemText inset primary="8 passage Boulay, 75017 Paris" />
                </ListItem>
                <ListItem button component="a"href="mailto:cthuault@gmail.com">
                    <ListItemIcon>
                        <EmailIcon />
                    </ListItemIcon>
                    <ListItemText inset primary="cthuault@gmail.com" />
                </ListItem>
                <ListItem button component="a" target="_blank" href="https://gitlab.com/ClemT">
                    <ListItemIcon>
                        <CodeIcon />
                    </ListItemIcon>
                    <ListItemText inset primary="Gitlab" />
                </ListItem>
            </List>    
        </CardContent>
        </Card>
      </div>

      </Grid>
      </ScrollableAnchor>
    );
  }
 
};
 
export default Contact;