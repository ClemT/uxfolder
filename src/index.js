import React from 'react';
import ReactDOM from 'react-dom';
import Header from './header/header';
import Home from './home/home';
import PorteFolio from './porteFolio/porteFolio';
import Photography from './photography/photography';
import Skill from './skill/skill';
import Cv from './cv/cv';
import Contact from './contact/contact';
import Footer from './footer/footer';
import './assets/common.scss';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import * as serviceWorker from './serviceWorker';


const theme = createMuiTheme({
    palette: {
      primary: { main: '#000' },
      secondary: { main: '#fff' }
    },
    typography: { useNextVariants: true },
});


function App() {
    return (
      <MuiThemeProvider theme={theme}>
        <Header />
        <main className="main">
            <Home />
            <PorteFolio />
            <Photography />
            <Skill />
            <Cv />
            <Contact />
            <Footer />
        </main>
      </MuiThemeProvider>
    );
}

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
