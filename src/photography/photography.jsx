import React from 'react';
import "./photography.scss"
import ScrollableAnchor from 'react-scrollable-anchor'
import Grid from '@material-ui/core/Grid';
import Carousel from 'nuka-carousel';
import Fab from '@material-ui/core/Fab';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';

class Photography extends React.Component {

    render() {
      return (
        <ScrollableAnchor id={'photography'} >
            <Grid container className="photography main-container">
                <Grid item xs={12}  >
                    <h3>Photographies</h3>
                </Grid>      
                <Carousel 
                    wrapAround = {true}
                    renderCenterLeftControls={({ previousSlide }) => (
                        <Fab size="small" onClick={previousSlide} aria-label="KeyboardArrowLeft">
                            <KeyboardArrowLeft />
                        </Fab>
                    )}
                    renderCenterRightControls={({ nextSlide }) => (
                        <Fab size="small" onClick={nextSlide} aria-label="KeyboardArrowRight">
                        <KeyboardArrowRight />
                        </Fab>
                    )} >
                        <div>
                            <img src={ require('../assets/imgs/photos/img1.jpg')} alt="" style={{ width : "100%" }} />
                        </div>
                        <div>
                            <img src={ require('../assets/imgs/photos/img2.jpg')} alt="" style={{ width : "100%" }}/>
                        </div>
                        <div>
                            <img src={ require('../assets/imgs/photos/img3.jpg')}alt="" style={{ width : "100%" }}/>
                        </div>
                        <div>
                            <img src={ require('../assets/imgs/photos/img4.jpg')} alt="" style={{ width : "100%" }}/>
                        </div>
                        <div>
                            <img src={ require('../assets/imgs/photos/img5.jpg')} alt="" style={{ width : "100%" }}/>
                        </div>
                        <div>
                            <img src={ require('../assets/imgs/photos/img6.jpg')} alt="" style={{ width : "100%" }}/>
                        </div>
                        <div>
                            <img src={ require('../assets/imgs/photos/img7.jpg')} alt="" style={{ width : "100%" }}/>
                        </div>
                        <div>
                            <img src={ require('../assets/imgs/photos/img8.jpg')} alt="" style={{ width : "100%" }}/>
                        </div>
                        <div>
                            <img src={ require('../assets/imgs/photos/img9.jpg')} alt="" style={{ width : "100%" }}/>
                        </div>
                        <div>
                            <img src={ require('../assets/imgs/photos/img10.jpg')} alt="" style={{ width : "100%" }}/>
                        </div>
                        <div>
                            <img src={ require('../assets/imgs/photos/img11.jpg')} alt="" style={{ width : "100%" }}/>
                        </div>
                        <div>
                            <img src={ require('../assets/imgs/photos/img12.jpg')}alt="" style={{ width : "100%" }}/>
                        </div>
                        <div>
                            <img src={ require('../assets/imgs/photos/img13.jpg')}alt="" style={{ width : "100%" }}/>
                        </div>
                        <div>
                            <img src={ require('../assets/imgs/photos/img14.jpg')} alt="" style={{ width : "100%" }}/>
                        </div>  
                        <div>
                            <img src={ require('../assets/imgs/photos/img15.jpg')} alt="" style={{ width : "100%" }}/>
                        </div>
                        <div>
                            <img src={ require('../assets/imgs/photos/img16.jpg')} alt="" style={{ width : "100%" }}/>
                        </div> 
                        <div>
                            <img src={ require('../assets/imgs/photos/img17.jpg')} alt="" style={{ width : "100%" }}/>
                        </div> 
                        <div>
                            <img src={ require('../assets/imgs/photos/img18.jpg')} alt="" style={{ width : "100%" }}/>
                        </div> 
                        <div>
                            <img src={ require('../assets/imgs/photos/img19.jpg')} alt="" style={{ width : "100%" }}/>
                        </div> 

                   </Carousel>
            </Grid>    
        </ScrollableAnchor>
        
        );
    }
    
}

export default Photography;