import React from 'react';
import "./footer.scss"

class Footer extends React.Component {

    render() {
      return (
        <footer>
            <p>© Ux folder by Clément Thuault (Beta)</p>
            <p>Fait avec ReactJS et Matérial-UI</p>
        </footer>
        
        );
    }
    
}

export default Footer;