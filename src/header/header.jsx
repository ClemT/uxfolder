import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import './header.scss';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

const styles = {
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
};

class Header extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      left: false,
      value: 0
     }
    this.handleHashChange = this.handleHashChange.bind(this);
  }
  
  componentDidMount() {
    window.addEventListener('hashchange', this.handleHashChange);
  };
  
  componentWillUnmount() {
    window.removeEventListener('hashchange', this.handleHashChange);
  };

  handleHashChange() {
    this.setState({
        hash : window.location.hash,
     });
    }
  toggleDrawer = (side, open) => () => {
    this.setState({
      [side]: open,
    });
  };

  render() {

    return (
      <div>
        <AppBar position="fixed">
            <Toolbar >
              <Grid container direction="row" justify="space-between" alignItems="center">
                <div className="logo">
                  <IconButton color="inherit" aria-label="Menu" onClick={this.toggleDrawer('left', true)}>
                    <MenuIcon />
                  </IconButton>
                  <Typography  color="inherit" className="logo-lbl">
                    UX folder <span className="by">by Clément Thuault</span>
                  </Typography>
                </div>
                  <List className="nav">
                    <ListItem button component="a" href="#home" className={ this.state.hash === "#home" ? "anchor-link active" : "anchor-link" } >
                      <ListItemText primary="Accueil" className="list-item" />
                    </ListItem>
                    <ListItem button component="a" href="#porteFolio" className={ this.state.hash === "#porteFolio" ? "anchor-link active" : "anchor-link" } >
                      <ListItemText primary="Portefolio" className="list-item" />
                    </ListItem>
                    <ListItem button component="a" href="#photography" className={ this.state.hash === "#photography" ? "anchor-link active" : "anchor-link" } >
                      <ListItemText primary="Photographie" className="list-item"/>
                    </ListItem>            
                    <ListItem button component="a" href="#skill" className={ this.state.hash === "#skill" ? "anchor-link active" : "anchor-link" } >
                      <ListItemText primary="Compétences" className="list-item" />
                    </ListItem>
                    <ListItem button component="a" href="#cv" className={ this.state.hash === "#cv"  ? "anchor-link active" : "anchor-link"  } >
                      <ListItemText primary="Cv" className="list-item" />
                    </ListItem>
                    <ListItem button component="a" href="#contact" className={ this.state.hash === "#contact"  ? "anchor-link active" : "anchor-link"  } >
                      <ListItemText primary="Contact" className="list-item" />
                    </ListItem>
                  </List>
              </ Grid>
            </Toolbar>
        </AppBar>

        <Drawer open={this.state.left} onClose={this.toggleDrawer('left', false)}>
          <List className="nav-burger">
            <ListItem button component="a" href="#home">
              <ListItemText primary="Accueil" />
            </ListItem>
            <ListItem button component="a" href="#porteFolio">
              <ListItemText primary="Porte-folio" />
            </ListItem>
            <ListItem button component="a" href="#photography">
              <ListItemText primary="Photographie" />
            </ListItem>            
            <ListItem button component="a" href="#skill">
              <ListItemText primary="Compétences" />
            </ListItem>
            <ListItem button component="a" href="#cv">
              <ListItemText primary="Cv" />
            </ListItem>
            <ListItem button component="a" href="#contact">
              <ListItemText primary="Contact" />
            </ListItem>
          </List>
        </Drawer>

      </div>
    );
  }
}

export default withStyles(styles)(Header);