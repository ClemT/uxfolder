import React from 'react';
import "./cv.scss";
import ScrollableAnchor from 'react-scrollable-anchor'
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import PrintIcon from '@material-ui/icons/Print';

class Cv extends React.Component {
    render() {
      return (
        <ScrollableAnchor id={'cv'} >
            <Grid container className="cv main-container">
                <Grid item xs={12}  >
                    <h3>Curriculum vitae</h3>
                </Grid> 
                <Grid item xs={12}  >
                    <div className="buttons">
                        <Button variant="contained" size="large" className="btn" href="/assets/pdf/Lettre-recommandation.pdf" target="_blank">
                            <PrintIcon  /> Lettre de recommandation (pdf)
                        </Button>
                        <Button variant="contained" size="large" className="btn" href="/assets/pdf/clement_Thuault.pdf" target="_blank">
                            <PrintIcon /> Cv en (pdf)
                        </Button>
                    </div>
                    <Paper className="paper">
                    <article className="header">
                    
                    <Grid container>
                        <Grid item xs={12} sm={4} md={3} className="contact" >
                            <h4>Clément Thuault</h4>
                            <p>Ui/ux designer <br/>Front-end developer </p>
                            <p>Tel : <a className="link" href="tel:0651715748">06 51 71 57 48</a></p> 
                            <p>Adresse : 8 passage boulay <br/>
                            75017 Paris France</p>
                            <p>Mail : <a className="link" href="mailto:cthuault@gmail.com">cthuault@gmail.comt</a></p>
                            <p>Url : <a className="link" href="http://uxfolder.fr">uxfolder.fr</a></p>
                        </Grid>
                        <Grid item sm={8} md={9} className="banner" >
                        </Grid>
                    </Grid>
                    </article>
                    <article className="container-cv">
                        <h4>Expériences</h4>
                        <h5>2013 - 2018 Front-end developer / Ui designer pour Orchestra</h5>
                        <p className="edito">
                        Conception, réalisation et maintien de sites b2c et b2b pour des agences de voyage en ligne. (Lastminute, Thomascook, Jettours, VP viaggi, Govoyage, Directours, Thalasseo, Aaccor, Lux*, Transavia, Voyamar, Carrefour viajes, Visiteurope, Celignes, Cdiscount, Scott Dunn ...)
                        </p>
                        <h5>2012 - 2013 Webdesigner / Ux designer pour lafrancedunordausud.fr</h5>
                        <p className="edito">
                        Mise à jour du site et publicité pour une agence de voyage en ligne. (publicités, newsletters, opérations comerciales ...)
                        </p>
                        <h5>2011 - 2012 Intégrateur / Webdesigner pour Netapsys</h5>
                        <p className="edito">
                        Conception et intégration de site pour une société d’ingénieur en informatique. (Safran, Ministère du travail, Ministère de la santé, Adopi, Police ...)
                        </p>
                        <h5>2010 - 2011 Webdesigner pour Net Stim (en alternance avec l’école)</h5>
                        <p className="edito">
                        Mise à jour du site et publicité pour les magasins Truffaut. (publicité, newsletter, opérations comerciales ...)
                        </p>
                        <h5>2009 - 2010 Webdesigner pour Smartagence (en alternance avec l’école)</h5>
                        <p className="edito">
                        Conception de charte graphique dans une agence de création. (Gmf, Cegelec, Demain TV, Afpa, Dsi volant, Ministére de la finance ...)
                        </p>
                        <h5>2007 - 2008 Webmaster pour Idcrea (en alternance avec l’école)</h5>
                        <p className="edito">
                        Assistant webmaster et web designer dans l’immobiler et la de-fiscalisation. (Espace vert et terrasse, Office Français du Patrimoine ...)
                        </p>
                    </article>

                    <article className="container-cv">
                        <h4>Ecoles</h4>
                        <h5>2010 - 2011 Front-end developer</h5>
                        <p className="edito">
                        Apprentissage des différents langages de programmation. IP-formation à Paris.
                        </p>
                        <h5>2009 - 2010 Web master</h5>
                        <p className="edito">
                        Apprentissage des outils de developement multimedia. Doranco à Paris.
                        </p>
                        <h5>2007 - 2008 Web designer</h5>
                        <p className="edito">
                        Apprentissage des outils de communication visuel. Efficom à Paris.
                        </p>
                        <h5>2005 - 2006 Designer</h5>
                        <p className="edito">
                        Classe préparatoire art plastique. Atelier EDTA-SORNAS à Paris.
                        </p>
                    </article>

                    <article className="container-cv">
                        <h4>Evénement</h4>
                        <p className="edito">
                        Participation au hackaton le printemp d’orchestra. <br/>
                        Expositions pendant les Nuits de la photo.<br/>
                        Marathons 20km de Paris.
                        </p>
                    </article>

                    <article className="container-cv">
                        <h4>Hobbies</h4>
                        <p className="edito">
                        Mon passe-temps préféré est la photographie et le voyage, j’aime beaucoup l’art en général et les nouvelles technologies, je fais super bien la cuisine végétarienne.
                        </p>
                    </article>
                    </Paper>
                </Grid>
           </Grid>     
        </ScrollableAnchor>
        );
    } 
}

export default Cv;