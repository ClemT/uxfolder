import React from 'react';
import "./porteFolio.scss";
import ScrollableAnchor from 'react-scrollable-anchor';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Tilt from 'react-tilt';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import MuiDialogActions from '@material-ui/core/DialogActions';

class PorteFolio extends React.Component {

    state = {
        open: false,
    }

    handleClickOpen = (e) => {
        this.setState({
          open: true,
          url : e.currentTarget.src,
          alt : e.currentTarget.alt,
          desc : e.currentTarget.nextSibling.innerHTML,
          link : e.currentTarget.parentElement.getElementsByClassName("link")[0].innerHTML,
        });
      }
    
    handleClose = () => {
        this.setState({ 
            open: false
         });
    }


    render() {

        const orchestra =  [ 
            { 
              id: '1',
              label: 'Orchestra',
              imgPath: require('../assets/imgs/book/orchestra.jpg'),
              desc: "Orchestra : integration et devellopement du site de l'agence sous worpress. © Orchestra",
              href: "http://www.orchestra.eu",
            },
            {
              id: '2',
              label: 'Visiteurope',
              imgPath: require('../assets/imgs/book/visiteurope.jpg'),
              desc: "Visiteurope : Refonte du site. © Orchestra",
              href: "http://www.visiteurope.fr",
            },
            {
              id: '3',
              label: 'Thomas cook',
              imgPath: require('../assets/imgs/book/thomasCook.jpg'),
              desc: "Thomas cook: Refonte du header et menu. Evolutions et corrections d'anomalies. © Orchestra",
              href: "http://www.thomascook.fr",
            },
            {
              id: '4',
              label: 'Lux',
              imgPath: require('../assets/imgs/book/lux.jpg'),
              desc: "Lux : Conception et intégration du tunnel de réservation. © Orchestra",
              href: "http://luxresorts.com",
            },
            {
              id: '5',
              label: 'Viajes',
              imgPath: require('../assets/imgs/book/viajes.jpg'),
              desc: "Viajes: Conception et intégration du tunnel de réservation. © Orchestra",
              href: "https://www.viajes.carrefour.es/",
            },
            {
                id: '6',
                label: 'Lastminute',
                imgPath: require('../assets/imgs/book/lastminute.jpg'),
                desc: "Lastminute : Conception et intégration du tunnel de réservation. © Orchestra",
                href: "http://lastminute.com",
              },
              {
                id: '7',
                label: 'Vente privé voyages',
                imgPath: require('../assets/imgs/book/vp.jpg'),
                desc: "Vente privé voyages : Conception et intégration du tunnel de réservation. © Orchestra",
              },
              {
                id: '8',
                label: 'Thalassa',
                imgPath: require('../assets/imgs/book/thalassa.jpg'),
                desc: "Thalassa : Conception et intégration du tunnel de réservation. © Orchestra",
              },
        ];

        const projet =  [ 
            { 
              id: '1',
              label: 'Carmen Ragosta',
              imgPath: require('../assets/imgs/book/carmenRagosta.jpg'),
              desc: "Carmen Ragosta : Conception et dévellopement du site.",
              href: "http://carmenragosta.com",
            },
            {
              id: '2',
              label: 'Megumi Nakanishi',
              imgPath: require('../assets/imgs/book/meguminakanishi.jpg'),
              desc: "Megumi Nakanishi : Conception et dévellopement du site.",
              href: "http://meguminakanish.com",
            },
            {
              id: '3',
              label: 'JVK',
              imgPath: require('../assets/imgs/book/jvk.jpg'),
              desc: "JVK : Conception et dévellopement du site.",
              href: "http://jakobvekaindustries.fr",
            },
            {
              id: '4',
              label: 'Style Research',
              imgPath: require('../assets/imgs/book/sr.jpg'),
              desc: "Style Research : Conception et dévellopement du site.",
            },
            {
              id: '5',
              label: 'SFH',
              imgPath: require('../assets/imgs/book/sfh.jpg'),
              desc: "SFH : Conception et dévellopement du site.",
            },
        ];

        const lfdnas =  [ 
            { 
              id: '1',
              label: 'La france du nord au sud',
              imgPath: require('../assets/imgs/book/ldnas1.jpg'),
              desc: "La france du nord au sud",
            },
            {
              id: '2',
              label: 'La france du nord au sud',
              imgPath: require('../assets/imgs/book/ldnas2.jpg'),
              desc: "La france du nord au sud",
            },
            {
              id: '3',
              label: 'La france du nord au sud',
              imgPath: require('../assets/imgs/book/ldnas3.jpg'),
              desc: "La france du nord au sud",
            },
            {
              id: '4',
              label: 'La france du nord au sud',
              imgPath: require('../assets/imgs/book/ldnas4.jpg'),
              desc: "La france du nord au sud",
            },
        ];

        const netapsys =  [ 
            { 
              id: '1',
              label: 'Safran musée',
              imgPath: require('../assets/imgs/book/safran.jpg'),
              desc: "Safran musée",
            },
            {
              id: '2',
              label: 'Ministère de la santé',
              imgPath: require('../assets/imgs/book/ministersante.jpg'),
              desc: "Ministère de la santé",
            },
            {
              id: '3',
              label: 'Rupture conventionelle',
              imgPath: require('../assets/imgs/book/rc.jpg'),
              desc: "Rupture conventionelle",
            },
            {
              id: '4',
              label: "Rupture conventionelle",
              imgPath: require('../assets/imgs/book/rc2.jpg'),
              desc: "Rupture conventionelle",
              href : "https://www.telerc.travail.gouv.fr/RuptureConventionnellePortailPublic/jsp/site/Portal.jsp"
            },
        ];

        const truffaut =  [ 
            { 
              id: '1',
              label: 'Truffaut',
              imgPath: require('../assets/imgs/book/truffaut1.jpg'),
              desc: "Truffaut",
            },
            {
              id: '2',
              label: 'Truffaut',
              imgPath: require('../assets/imgs/book/truffaut2.jpg'),
              desc: "Truffaut",
            },
            {
              id: '3',
              label: 'Truffaut',
              imgPath: require('../assets/imgs/book/truffaut3.jpg'),
              desc: "Truffaut",
            },
            {
              id: '4',
              label: "Truffaut",
              imgPath: require('../assets/imgs/book/truffaut4.jpg'),
              desc: "Truffaut",
            },
        ];

        const smartagence =  [ 
            { 
              id: '1',
              label: 'GMF',
              imgPath: require('../assets/imgs/book/gmf.jpg'),
              desc: "GMF",
            },
            {
              id: '2',
              label: 'GMF',
              imgPath: require('../assets/imgs/book/gmf2.jpg'),
              desc: "GMF",
            },
            {
              id: '3',
              label: 'Cegelec',
              imgPath: require('../assets/imgs/book/cegelec.jpg'),
              desc: "Cegelec",
            },
            {
              id: '4',
              label: "DSI Volant",
              imgPath: require('../assets/imgs/book/dsi.jpg'),
              desc: "DSI Volant",
            },
        ];
          
      return (
        <ScrollableAnchor id={'porteFolio'} >
            <Grid container className="portefolio main-container">
                <Grid item xs={12}  >
                    <h3>Porte-folio</h3>
                    <h4>Orchestra</h4>
                </Grid>
                { orchestra.map( (data) => 
                    <Grid item xs={12}  sm={6} md={3} key={data.id}  >
                        <Tilt className="Tilt" options={{ max : 25, scale: 1 }}>
                            <Paper elevation={1} square={false} className="paper" >
                                <img onClick={this.handleClickOpen} width="100%" src={data.imgPath } alt={data.label } className="show-modal"/>
                                <div className="description hidden">
                                    {data.desc }
                                </div>
                                <div className="link hidden"> {data.href} </div>
                            </Paper>
                        </Tilt>
                    </Grid>
                )}
                <Grid item xs={12}  >
                    <h4>Projets</h4>
                </Grid>
                { projet.map( (data) => 
                    <Grid item xs={12}  sm={6} md={3}  key={data.id}  >
                        <Tilt className="Tilt" options={{ max : 25, scale: 1 }}>
                            <Paper elevation={1} square={false} className="paper" >
                                <img onClick={this.handleClickOpen} width="100%" src={data.imgPath } alt={data.label } className="show-modal"/>
                                <div className="description hidden">
                                    {data.desc }
                                </div>
                                <div className="link hidden"> {data.href} </div>
                            </Paper>
                        </Tilt>
                    </Grid>
                )}
                <Grid item xs={12}  >
                    <h4>La france du nord au sud</h4>
                </Grid>
                { lfdnas.map( (data) => 
                    <Grid item xs={12}  sm={6} md={3}  key={data.id}  >
                        <Tilt className="Tilt" options={{ max : 25, scale: 1 }}>
                            <Paper elevation={1} square={false} className="paper" >
                                <img onClick={this.handleClickOpen} width="100%" src={data.imgPath } alt={data.label } className="show-modal"/>
                                <div className="description hidden">
                                    {data.desc }
                                </div>
                                <div className="link hidden"> {data.href} </div>
                            </Paper>
                        </Tilt>
                    </Grid>
                )}
                <Grid item xs={12}  >
                    <h4>Netapsys</h4>
                </Grid>
                { netapsys.map( (data) => 
                    <Grid item xs={12}  sm={6} md={3} key={data.id}  >
                        <Tilt className="Tilt" options={{ max : 25, scale: 1 }}>
                            <Paper elevation={1} square={false} className="paper" >
                                <img onClick={this.handleClickOpen} width="100%" src={data.imgPath } alt={data.label } className="show-modal"/>
                                <div className="description hidden">
                                    {data.desc }
                                </div>
                                <div className="link hidden"> {data.href} </div>
                            </Paper>
                        </Tilt>
                    </Grid>
                )}
                <Grid item xs={12}  >
                    <h4>Truffaut</h4>
                </Grid>
                { truffaut.map( (data) => 
                    <Grid item xs={12}  sm={6} md={3}  key={data.id}  >
                        <Tilt className="Tilt" options={{ max : 25, scale: 1 }}>
                            <Paper elevation={1} square={false} className="paper" >
                                <img onClick={this.handleClickOpen} width="100%" src={data.imgPath } alt={data.label } className="show-modal"/>
                                <div className="description hidden">
                                    {data.desc }
                                </div>
                                <div className="link hidden"> {data.href} </div>
                            </Paper>
                        </Tilt>
                    </Grid>
                )}
                <Grid item xs={12}  >
                    <h4>Smart agence</h4>
                </Grid>
                { smartagence.map( (data) => 
                    <Grid item xs={12} sm={6} md={3} key={data.id}  >
                        <Tilt className="Tilt" options={{ max : 25, scale: 1 }}>
                            <Paper elevation={1} square={false} className="paper" >
                                <img onClick={this.handleClickOpen} width="100%" src={data.imgPath } alt={data.label } className="show-modal"/>
                                <div className="description hidden">
                                    {data.desc }
                                </div>
                                <div className="link hidden"> {data.href} </div>
                            </Paper>
                        </Tilt>
                    </Grid>
                )}
                <Dialog className="dialog"onClose={this.handleClose} aria-labelledby="customized-dialog-title" open={this.state.open} fullWidth={true} maxWidth = {'md'} >
                    <MuiDialogTitle id="customized-dialog-title">
                        <span className="description">
                                { this.state.open? <> { this.state.desc } </> : "" }<br/>
                        </span>
                        <IconButton aria-label="Close" className="btn-close" onClick={this.handleClose}>
                            <CloseIcon />
                        </IconButton>
                    </MuiDialogTitle>
                    <MuiDialogContent>
                        { this.state.open? <img width="100%" src={ this.state.url } alt={ this.state.alt } /> : "" }
                    </MuiDialogContent>
                    <MuiDialogActions className="dialog-action">
                        <Button component="a" href={ this.state.link } target="_blank"   className="btn-action">
                            Voir le site
                        </Button>
                    </MuiDialogActions>
                </Dialog>
            </Grid>
        </ScrollableAnchor>
        );
    }
    
}

export default PorteFolio;