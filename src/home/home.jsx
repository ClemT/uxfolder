import React from 'react';
import './home.scss';
import ScrollableAnchor from 'react-scrollable-anchor'
import Carousel from 'nuka-carousel';
import Fab from '@material-ui/core/Fab';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import KeyboardArrowDown from '@material-ui/icons/KeyboardArrowDown';
import Tilt from 'react-tilt';


class Home extends React.Component {


    
constructor(props) {
  super(props);
  this.state = {
    scrollTop : 0,
    imgHeight: window.innerHeight
   }
  this.handleScroll = this.handleScroll.bind(this);
  this.handleResize = this.handleResize.bind(this);
  
}

componentDidMount() {
  window.addEventListener('scroll', this.handleScroll);
  window.addEventListener('resize', this.handleResize);
};

componentWillUnmount() {
  window.removeEventListener('scroll', this.handleScroll);
  window.removeEventListener('resize', this.handleResize);
};

handleResize() {
    this.setState({
        imgHeight : window.innerHeight
     });
}

handleScroll() {

  this.setState({
    scrollTop :  Math.max(window.pageYOffset, document.documentElement.scrollTop, document.body.scrollTop)
   });
  
}

    render() {
      return (
        <ScrollableAnchor id={'home'} >
            <section className="home">
            <Carousel 
            wrapAround = {true}
            autoplay = {true}
            renderCenterLeftControls={({ previousSlide }) => (
                <Fab size="small" onClick={previousSlide} aria-label="KeyboardArrowLeft">
                    <KeyboardArrowLeft />
                </Fab>
              )}
              renderCenterRightControls={({ nextSlide }) => (
                <Fab size="small" onClick={nextSlide} aria-label="KeyboardArrowRight">
                   <KeyboardArrowRight />
                </Fab>
              )}
            
            className="paralax" style={{ height: this.state.imgHeight }}>
                <div style={{ height: (this.state.imgHeight) * 2 }} className="toto">
                    <div className="brand">
                        <Tilt className="Tilt" options={{ scale: 1 }}>
                            <div className="Tilt-inner">
                                <h2 className="js-tilt">ui/ux <br/>Designer</h2>
                            </div>
                        </Tilt>
                        <p>Voir le porte-folio :</p>
                        <Fab size="small" color="primary" aria-label="KeyboardArrowDown" href="#porteFolio">
                            <KeyboardArrowDown />
                        </Fab>
                    </div>
                    <img className="paralaxImg" style={{bottom: this.state.scrollTop + "px" }} src="http://uxfolder.fr/imgs/bg1.jpg" width="100%" alt=""/>
                </div>
                <div style={{ height: (this.state.imgHeight) * 2 }} className="toto">
                    <div className="brand">
                        <Tilt className="Tilt" options={{ scale: 1 }}>
                            <div className="Tilt-inner">
                                <h2 className="js-tilt">Front-end<br/>Developer</h2>
                            </div>
                        </Tilt>
                        <p>Voir les compétences :</p>
                        <Fab size="small" color="primary" aria-label="KeyboardArrowDown" href="#porteFolio">
                            <KeyboardArrowDown />
                        </Fab>
                    </div>
                    <img className="paralaxImg" style={{bottom: this.state.scrollTop + "px" }} src="http://uxfolder.fr/imgs/bg3.jpg" width="100%" alt=""/>
                </div>
                <div style={{ height: (this.state.imgHeight) * 2 }} className="toto">
                    <div className="brand">
                        <Tilt className="Tilt" options={{ scale: 1 }}>
                            <div className="Tilt-inner">
                                <h2 className="js-tilt" style={{ color: "#fff" }}>Photographe</h2>
                            </div>
                        </Tilt>
                        <p style={{ color: "#fff" }} >Voir les photographies :</p>
                        <Fab size="small" color="secondary" aria-label="KeyboardArrowDown" href="#porteFolio">
                            <KeyboardArrowDown />
                        </Fab>
                    </div>
                    <img className="paralaxImg" style={{bottom: this.state.scrollTop + "px" }} src="http://uxfolder.fr/imgs/bg2.jpg" width="100%" alt=""/>
                </div>

            </Carousel>
            </section>
        </ScrollableAnchor>
        );
    }
    
}

export default Home;